﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MoneyTransfer.Models
{
    public class User : IdentityUser
    {
        [Display(Name = "Лицевой счет: ")]
        public int PersonalAccount { get; set; }

        public decimal Balance { get; set; }
    }
}
