﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }
        public decimal TransactionSum { get; set; }
        public DateTime TransactionDate { get; set; }

        public int RecipientId { get; set; }
        public int SenderId { get; set; }
    }
}
