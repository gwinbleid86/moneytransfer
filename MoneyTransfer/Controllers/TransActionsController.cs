﻿using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransactionsController : Controller
    {
        private ApplicationContext _context;
        private UserManager<User> _userManager;

        public TransactionsController(ApplicationContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }


        // GET: TransActions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Transactions.ToListAsync());
        }

        // GET: TransActions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transAction = await _context.Transactions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (transAction == null)
            {
                return NotFound();
            }

            return View(transAction);
        }

        // GET: TransActions/Create
        public IActionResult AddBalance(/*int pers_acc, double sum*/)//надо ли сюда что-то принимать
        {
            return View();
        }

        // POST: TransActions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBalance(AddBalanceViewModel model)
        {
            User user = _context.Users.FirstOrDefault(u => u.PersonalAccount == model.PersonalAccount);
            if (user != null)
            {
                user.Balance = user.Balance + model.Sum;

                _context.Update(user);
                _context.SaveChanges();
                return RedirectToAction("AddBalanceSuccess");
            }
            return RedirectToAction("Error");

            return View();
        }
        public IActionResult Transact()
        {
            return View();
        }

        [HttpPost]
        // GET: TransActions/Edit/5
        public async Task<IActionResult> Transact(TransactionViewModel model)
        {
            User senderUser = _context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            User recipienUser = _context.Users.FirstOrDefault(u => u.PersonalAccount == model.RecipientId);
            if (senderUser.Balance >= model.TransactionSum)
            {
                Transaction transaction = new Transaction()
                {
                    TransactionSum = model.TransactionSum,
                    RecipientId = model.RecipientId,
                    SenderId = senderUser.PersonalAccount,
                    TransactionDate = DateTime.Now
                };
                senderUser.Balance = senderUser.Balance - model.TransactionSum;
                recipienUser.Balance = recipienUser.Balance + model.TransactionSum;
                _context.Add(transaction);
                _context.SaveChanges();
                return View("TransactSuccess", transaction);
            }


            return View();
        }

        // POST: TransActions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Sum,UserId")] Transaction transAction)
        {
            if (id != transAction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transAction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransActionExists(transAction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(transAction);
        }
        // GET: TransActions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transAction = await _context.Transactions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (transAction == null)
            {
                return NotFound();
            }

            return View(transAction);
        }

        // POST: TransActions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transAction = await _context.Transactions.SingleOrDefaultAsync(m => m.Id == id);
            _context.Transactions.Remove(transAction);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransActionExists(int id)
        {
            return _context.Transactions.Any(e => e.Id == id);
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult AddBalanceSuccess()
        {
            return View();
        }
    }
}    