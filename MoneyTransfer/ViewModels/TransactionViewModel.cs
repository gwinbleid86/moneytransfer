﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class TransactionViewModel
    {
        [Display(Name = "Сумма")]
        public decimal TransactionSum { get; set; }
        [Display(Name = "Лицевой счет получателя")]
        public int RecipientId { get; set; }

    }
}
