﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class AddBalanceViewModel
    {
        public int PersonalAccount { get; set; }
        public decimal Sum { get; set; }
    }
}
