﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Repositories
{
    public class CodeGenerator
    {
        public int Generator()
        {
            //            StringBuilder builder = new StringBuilder();
            //            Enumerable
            //                .Range(65, 26)
            //                .Select(e => ((char)e).ToString())
            //                .Concat(Enumerable.Range(97, 26).Select(e => ((char)e).ToString()))
            //                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
            //                .OrderBy(e => Guid.NewGuid())
            //                .Take(6)
            //                .ToList().ForEach(e => builder.Append(e));
            //            string id = builder.ToString();
            //
            //            return id;

            Random rnd = new Random();
            int code = Convert.ToInt32(string.Format("{0:D6}", rnd.Next(0, 1000000)));

            return code;
        }

        public bool Exist(List<int> code, int c)
        {
            if (code.Contains(c)) return false;
            return true;
        }
    }
}
