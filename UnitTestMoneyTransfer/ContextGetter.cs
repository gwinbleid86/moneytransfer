﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using MoneyTransfer;

namespace UnitTestMoneyTransfer
{
    class ContextGetter
    {
        private static ApplicationContext context;

        public static ApplicationContext GetApplicationContext()
        {
            if (context == null)
            {
                context = new ApplicationContext(@"Server=(localdb)\\mssqllocaldb;Database=MoneyTransferTest;Trusted_Connection=True;MultipleActiveResultSets=true");
            }

            return context;
        }
    }
}
